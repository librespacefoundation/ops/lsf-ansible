---
- name: Install nginx package
  ansible.builtin.package:
    name: 'nginx'
    state: 'present'
  register: res
  until: res is success
  retries: '{{ package_retries }}'
  delay: '{{ package_delay }}'
  become: true
- name: Enable nginx
  ansible.builtin.service:
    name: 'nginx'
    enabled: true
  become: true
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
- name: Deny access to default site
  ansible.builtin.copy:
    src: 'etc/nginx/default.d/444.conf'
    dest: '/etc/nginx/default.d/444.conf'
    mode: '644'
  become: true
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  when: >-
    nginx is defined
    and not (nginx.default_site | default(true))
- name: Allow access to default site
  ansible.builtin.file:
    path: '/etc/nginx/default.d/444.conf'
    state: 'absent'
  become: true
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  when: >-
    nginx is defined
    and (nginx.default_site | default(true))
- name: Copy nginx default site include file
  ansible.builtin.copy:
    src: '{{ item.file }}'
    dest: '/etc/nginx/default.d/{{ item.file | basename }}'
    mode: '644'
  become: true
  when: >-
    item.state | default('present') | lower == 'present'
    and nginx.defaults is not none
    and item.file is defined
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  with_items: '{{ (nginx.defaults if nginx is defined) | default([]) }}'
- name: Template out nginx default site include file
  ansible.builtin.template:
    src: '{{ item.template }}'
    dest: '/etc/nginx/default.d/{{ item.template | basename | regex_replace(".j2$") }}'
    mode: '644'
  become: true
  when: >-
    item.state | default('present') | lower == 'present'
    and nginx.defaults is not none
    and item.template is defined
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  with_items: '{{ (nginx.defaults if nginx is defined) | default([]) }}'
- name: Remove nginx default site include file
  ansible.builtin.file:
    path: '/etc/nginx/default.d/{{ item.file | basename }}'
    state: 'absent'
  become: true
  when: >-
    item.state | default('present') | lower == 'absent'
    and nginx.defaults is not none
    and item.file is defined
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  with_items: '{{ (nginx.defaults if nginx is defined) | default([]) }}'
- name: Remove nginx default site include file template
  ansible.builtin.file:
    path: '/etc/nginx/default.d/{{ item.template | basename | regex_replace(".j2$") }}'
    state: 'absent'
  become: true
  when: >-
    item.state | default('present') | lower == 'absent'
    and nginx.defaults is not none
    and item.template is defined
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  with_items: '{{ (nginx.defaults if nginx is defined) | default([]) }}'
- name: Copy nginx configuration
  ansible.builtin.copy:
    src: '{{ item.file }}'
    dest: '/etc/nginx/conf.d/{{ item.file | basename }}'
    mode: '644'
  become: true
  when: >-
    item.state | default('present') | lower == 'present'
    and nginx.confs is not none
    and item.file is defined
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  with_items: '{{ (nginx.confs if nginx is defined) | default([]) }}'
- name: Template out nginx configuration
  ansible.builtin.template:
    src: '{{ item.template }}'
    dest: '/etc/nginx/conf.d/{{ item.template | basename | regex_replace(".j2$") }}'
    mode: '644'
  become: true
  when: >-
    item.state | default('present') | lower == 'present'
    and nginx.confs is not none
    and item.template is defined
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  with_items: '{{ (nginx.confs if nginx is defined) | default([]) }}'
- name: Remove nginx configuration file
  ansible.builtin.file:
    path: '/etc/nginx/conf.d/{{ item.file | basename }}'
    state: 'absent'
  become: true
  when: >-
    item.state | default('present') | lower == 'absent'
    and nginx.confs is not none
    and item.file is defined
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  with_items: '{{ (nginx.confs if nginx is defined) | default([]) }}'
- name: Remove nginx configuration template
  ansible.builtin.file:
    path: '/etc/nginx/conf.d/{{ item.template | basename | regex_replace(".j2$") }}'
    state: 'absent'
  become: true
  when: >-
    item.state | default('present') | lower == 'absent'
    and nginx.confs is not none
    and item.template is defined
  notify:
    - 'Test nginx configuration'
    - 'Restart nginx'
  with_items: '{{ (nginx.confs if nginx is defined) | default([]) }}'
- name: Create www directory
  ansible.builtin.file:
    path: '/var/www'
    state: 'directory'
    mode: '755'
  become: true
- name: Synchronize nginx sites
  ansible.posix.synchronize:
    archive: false
    times: true
    delete: true
    recursive: true
    src: '{{ item.src }}'
    dest: '/var/www/{{ item.src | basename }}'
  become: true
  when: >-
    item.state | default('present') | lower == 'present'
    and nginx.www is not none
  with_items: '{{ (nginx.www if nginx is defined) | default([]) }}'
- name: Remove nginx sites
  ansible.builtin.file:
    path: '/var/www/{{ item.src | basename }}'
    state: 'absent'
  become: true
  when: >-
    item.state | default('present') | lower == 'absent'
    and nginx.www is not none
  with_items: '{{ (nginx.www if nginx is defined) | default([]) }}'
- name: Configure nginx network connect SELinux boolean
  ansible.posix.seboolean:
    name: 'httpd_can_network_connect'
    state: '{{ nginx.httpd_can_network_connect }}'
    persistent: true
  become: true
  when: >-
    nginx is defined
    and nginx.httpd_can_network_connect is defined
    and ansible_selinux.status == 'enabled'
